tinyceres
============

tinyceres is a small template library for solving Nonlinear Least Squares problems, created from small subset of [ceres-solver](http://ceres-solver.org/) - mainly TinySolver and its dependencies. It was created as a dependency for [Monado](https://monado.freedesktop.org/) for real-time optical hand tracking.
